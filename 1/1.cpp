#include <iostream>
#include <memory>
#include <thread>
#include <mutex>
#include <shared_mutex>

struct Shareable : std::enable_shared_from_this<Shareable> {
  template <typename Down>
  std::shared_ptr<Down> getSharedFromThisAs() {
    return std::dynamic_pointer_cast<Down>(shared_from_this());
  }

  template <typename Down>
  std::shared_ptr<Down> getSharedFromThisAs() const {
    return std::dynamic_pointer_cast<Down>(shared_from_this());
  }
  
  virtual ~Shareable() = default;
};

struct A : Shareable {
  virtual void setB(std::shared_ptr<Shareable> b) = 0;
};

struct B : Shareable {
  virtual void setA(std::shared_ptr<Shareable> a) = 0;
};

struct AImpl : A {
  std::recursive_mutex mutex;
  std::shared_ptr<B> b;
  
  void setB(std::shared_ptr<Shareable> b) {
    std::lock_guard lock(mutex);
    
    auto newB = b->getSharedFromThisAs<B>();
    
    if (newB) this->b = newB;
  }
  
  static std::shared_ptr<A> create() {
    return std::make_shared<AImpl>();
  }
};

struct BImpl : B {
  mutable std::shared_mutex mutex;
  std::shared_ptr<A> a;
  
  void setA(std::shared_ptr<Shareable> a) {
    std::unique_lock lock(mutex);
    
    auto newA = a->getSharedFromThisAs<A>();
    
    if (newA) this->a = newA;
  }
  
  static std::shared_ptr<B> create() {
    return std::make_shared<BImpl>();
  }
};

struct C : std::enable_shared_from_this<C> {
  std::shared_ptr<A> a;
  std::shared_ptr<B> b;
  
  C(std::shared_ptr<A> a, std::shared_ptr<B> b): a{std::move(a)}, b{std::move(b)} {}
  
  static std::shared_ptr<C> create() {
    auto a = AImpl::create();
    auto b = BImpl::create();
    
    a->setB(b);
    b->setA(a);
    
    return std::shared_ptr<C>(new C(a, b));
  }
};

int main() {
    auto c = C::create();
}
